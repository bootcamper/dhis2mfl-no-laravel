<?php

	include 'config.php';

	$mclCode = mysqli_real_escape_string($conn,$_GET['mclCode']);
	$communityUnitsName = mysqli_real_escape_string($conn,$_GET['communityUnitsName']);
	$parentID = array('id'=>mysqli_real_escape_string($conn,$_GET['parentID']));
	$openingDate = date('Y-m-d');

	$json = array(array('code'=> $mclCode,'name' => $communityUnitsName, 'shortName' =>$communityUnitsName, 'parent' => $parentID, 'openingDate'=>$openingDate));
	$orgUnits = array('organisationUnits'=>$json);

	$jsonData = json_encode($orgUnits);


	$ch = curl_init($metadataUrl);
    curl_setopt($ch, CURLOPT_HTTPHEADER,array('Content-Type: application/json'));
    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
	curl_setopt($ch, CURLOPT_POSTFIELDS,$jsonData);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);                                                                      
	curl_setopt($ch, CURLOPT_USERPWD, "$username:$password");
	$result = curl_exec($ch);

	echo $result;

?>