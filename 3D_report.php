<?php

    include 'config.php';
        $facilityQuery = "SELECT * FROM dhis_facilities WHERE code = 'Unassigned'";
        $result = mysqli_query($conn,$facilityQuery);
        $facilitiesCode = mysqli_num_rows($result);
        $communityQuery = "SELECT * FROM dhis_community_units WHERE code = 'Unassigned' OR length(dhis_community_units.code) != 6";
        $cuResult = mysqli_query($conn, $communityQuery);
        $communityCode = mysqli_num_rows($cuResult);
        $compareQuery = "SELECT Name,Code,County,Constituency FROM mfl_facilities WHERE mfl_facilities.Code NOT IN (SELECT code from dhis_facilities) AND mfl_facilities.Name NOT IN (select name FROM dhis_facilities where code = 'Unassigned')";
        $compaResult = mysqli_query($conn, $compareQuery);
        $compFacilities = mysqli_num_rows($compaResult);

        $compareCommunityUnits = "SELECT Name,Code,Facility, SubCounty FROM mfl_community_units WHERE mfl_community_units.Code NOT IN (SELECT code from dhis_community_units) AND mfl_community_units.Name NOT IN (select name FROM dhis_facilities where code = 'Unassigned') ORDER BY Name";
        $compaCommunity = mysqli_query($conn, $compareCommunityUnits);
        $compCommunityResults = mysqli_num_rows($compaCommunity);
        $invalid = "SELECT name, code, id, level FROM dhis_invalid_facilities";
        $retrieve = mysqli_query($conn, $invalid);
        $invalidFacilities = mysqli_num_rows($retrieve);

?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html>

    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        <title>amCharts examples</title>
        <link rel="stylesheet" href="amcharts/style.css" type="text/css">
        <script src="amcharts/amcharts/amcharts.js" type="text/javascript"></script>
        <script src="amcharts/amcharts/serial.js" type="text/javascript"></script>

        <script>
            var chart;

            var chartData = [
                {
                    "variable": "Facilities Without MFL Codes",
                    "numbers": <?php echo $facilitiesCode; ?>,
                    "color": "#FF0F00"
                },
                {
                    "variable": "Community Units without Codes",
                    "numbers": <?php echo $communityCode; ?>,
                    "color": "#FF6600"
                },
                {
                    "variable": "Facilities Not in DHIS",
                    "numbers": <?php echo $compFacilities; ?>,
                    "color": "#FF9E01"
                },
                {
                    "variable": "Community Units Not in DHIS",
                    "numbers": <?php echo $compCommunityResults;?>,
                    "color": "#FCD202"
                },
                {
                    "variable": "Facilites and Community Units in Wrong Hierarchy",
                    "numbers": <?php echo $invalidFacilities;?>,
                    "color": "#F8FF01"
                }
            ];


            AmCharts.ready(function () {
                // SERIAL CHART
                chart = new AmCharts.AmSerialChart();
                chart.dataProvider = chartData;
                chart.categoryField = "variable";
                // the following two lines makes chart 3D
                chart.depth3D = 20;
                chart.angle = 30;

                // AXES
                // category
                var categoryAxis = chart.categoryAxis;
                categoryAxis.labelRotation = 90;
                categoryAxis.dashLength = 5;
                categoryAxis.gridPosition = "start";

                // value
                var valueAxis = new AmCharts.ValueAxis();
                valueAxis.title = "Number of Facilities or Community Units";
                valueAxis.dashLength = 5;
                chart.addValueAxis(valueAxis);

                // GRAPH
                var graph = new AmCharts.AmGraph();
                graph.valueField = "numbers";
                graph.colorField = "color";
                graph.balloonText = "<span style='font-size:14px'>[[category]]: <b>[[value]]</b></span>";
                graph.type = "column";
                graph.lineAlpha = 0;
                graph.fillAlphas = 1;
                chart.addGraph(graph);

                // CURSOR
                var chartCursor = new AmCharts.ChartCursor();
                chartCursor.cursorAlpha = 0;
                chartCursor.zoomable = false;
                chartCursor.categoryBalloonEnabled = false;
                chart.addChartCursor(chartCursor);

                chart.creditsPosition = "top-right";


                // WRITE
                chart.write("chartdiv");
            });
        </script>
    </head>

    <body>
        

        <div class="container-fluid">
            <div class="row">
                <div id="chartdiv" style="width: 1100px; height: 700px;"></div> 
            </div>
        </div>
    </body>


</html>