<?php
     	
        include 'config.php';

        $query = "SELECT mfl_facilities.code,mfl_facilities.Name,dhis_facilities.Name AS dhis,dhis_facilities.parent_name AS parentname,dhis_facilities.id AS id 
        FROM dhis.dhis_facilities INNER JOIN mfl_facilities ON dhis_facilities.Code=mfl_facilities.code 
        WHERE dhis_facilities.name != mfl_facilities.Name";
        
        $result = mysqli_query($conn,$query);
        $dhisnames = mysqli_fetch_all($result,MYSQLI_ASSOC);
        echo json_encode($dhisnames);

?>  
