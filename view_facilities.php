<?php

	include 'config.php';

	if($_GET['type'] == 0){
		$query = "SELECT dhis_facilities.code,dhis_facilities.name,dhis_facilities.parent_name ,dhis_sub_counties.parent_name  as county FROM dhis.dhis_facilities INNER JOIN dhis.dhis_sub_counties on dhis_facilities.parent_id=dhis_sub_counties.id";

	}else if($_GET['type'] == 1){
		$query = "SELECT dhis_facilities.code,dhis_facilities.name,dhis_facilities.parent_name ,dhis_sub_counties.parent_name as county FROM dhis.dhis_facilities INNER JOIN dhis.dhis_sub_counties on dhis_facilities.parent_id=dhis_sub_counties.id WHERE dhis_facilities.code != 'Unassigned' AND length(dhis_facilities.code) = 5";
		
	}else if($_GET['type'] == 2){
		$query = "SELECT dhis_facilities.code,dhis_facilities.name,dhis_facilities.parent_name ,dhis_sub_counties.parent_name as county FROM dhis.dhis_facilities INNER JOIN dhis.dhis_sub_counties on dhis_facilities.parent_id=dhis_sub_counties.id WHERE dhis_facilities.code = 'Unassigned' OR length(dhis_facilities.code) != 5";
	}
	
	$result = mysqli_query($conn,$query);
	$facilities = mysqli_fetch_all($result,MYSQLI_ASSOC);
    echo json_encode($facilities);

?>