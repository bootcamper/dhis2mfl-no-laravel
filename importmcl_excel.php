<?php
$uploadedStatus = 0;
if ( isset($_POST["submit"]) )
  {
    if ( isset($_FILES["fileInput"]))
    {
    //if there was an error uploading the fileInput
      if ($_FILES["fileInput"]["error"] > 0)
        {
          echo "Return Code: " . $_FILES["fileInput"]["error"] . "<br />";
        }
        else
          {
            if (file_exists($_FILES["fileInput"]["name"]))
              {
                unlink($_FILES["fileInput"]["name"]);
              }
              $storagename = "Facility Export Material List";
              move_uploaded_file($_FILES["fileInput"]["tmp_name"],  $storagename);
              $uploadedStatus = 1;
          }
    }
        else
          {
            echo "No file selected <br />";
          }
  }
?>


<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>DHIS -> MFL</title>
	<link href="css/mycss.css" rel="stylesheet">
	<link rel="stylesheet" href="css/normalize.css">
    <link rel="stylesheet" href="font-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" type="text/css" href="DataTables/datatables.min.css"/>

</head>

<body>

<nav class="navbar navbar-inverse">
	<div class="container-fluid">

		<div class="navbar-header">
	      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
	        <span class="sr-only">Toggle navigation</span>
	        <span class="icon-bar"></span>
	        <span class="icon-bar"></span>
	        <span class="icon-bar"></span>
	      </button>
	      <a class="navbar-brand" href="index.php">DHIS2MFL</a>
	    </div>

		<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
	      	<ul class="nav navbar-nav">
				
				<!-- Dropdown list -->

	        	<li class="dropdown">
	        		<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Facilities<span class="caret"></span></a>
			            <ul class="dropdown-menu">
			            	<li><a href="#" class="updateFacilityNames">Update Facilities</a></li>
			            	<li><a href="#" class="facilityNoMfl">Facilities without MFL codes</a></li>			            	
				            <li><a href="#" class="compareFacilities">Compare Facilities</a></li>
				            <li><a href="#" class="facilitiesView">View Facilities</a></li>				           				           
				            <li role="separator" class="divider"></li>
				            <li><a href="#">Remove Facilities</a></li>
			            </ul>
	        	</li>

	        	<li class="dropdown">
	        		<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Community Health Units<span class="caret"></span></a>
			            <ul class="dropdown-menu">
			            	<li><a href="#">Update Community Health Units</a></li>
				            <li><a href="#" class="compareCommunityUnits">Compare Community Health Units</a></li>
				            <li><a href="#" class="communityUnitsView">View Community Health Units</a></li>				          
				            <li role="separator" class="divider"></li>
				            <li><a href="#">Remove Community Health Units</a></li>
			            </ul>
	        	</li>
				<li class="dropdown">
	        		<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Import<span class="caret"></span></a>
			            <ul class="dropdown-menu">
			            	<li><a href="import_excel.php" >Import Facilites</a></li>				          				          
				            <li role="separator" class="divider"></li>
				            <li><a href="importmcl_excel.php">Import Community Health Units</a></li>
			            </ul>
	        	</li>
	        	<li class="dropdown">
	        		<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Track Facilities<span class="caret"></span></a>
			            <ul class="dropdown-menu">
			            	<li><a href="#" class="viewCHUs">Community Units to be Tracked</a></li>
				            <li><a href="#" class="viewFacilitiesTrack">Facilities to be Tracked</a></li>
				            <li><a href="#" class="viewLevelSix">Facilities in the Wrong Hierarchy Level</a></li>
			            </ul>
	        	</li>
	        	<li><a href="docs\_build\html\index.html">Documentation</a></li>
	        	<li><a href="newdash.php">Dashboard</a></li>
	      	</ul>

	    </div>
	</div>
</nav>


<div id="pageContent">


	<div class="container">
		<!-- Main Header -->
		<div class="jumbotron">
			<h1>Community Health Unit List Import</h1>
			<p>Please upload an Excel File containing the latest Community Health Unit List data.</p>

			<form class="form-horizontal" method="POST" enctype="multipart/form-data" action="">

				<!-- Input Group for file upload -->
				<div class="form-group">
					<div class="col-sm-6">
					    <div class="input-group">
					      <input type="text" id="fileName" class="form-control" placeholder="Please select a file..." disabled>
					      	<span class="input-group-btn">
				        		<button class="btn btn-primary" type="button" id="uploadButton"><i class="fa fa-upload"></i></button>
				        		<input type="file" name="fileInput" id="fileInput">		        	
					      	</span>
					    </div>				  	
					</div>
				</div>

				<!-- Import Button -->
			  	<div class="form-group">
				    <div class="col-sm-10">
				      <button type="submit" name="submit" id="btnImport" class="btn btn-info btn-lg">Import Facilities</button>
				    </div>
			  	</div>

			</form>
		</div>


	</div>


</div>




<script src="js/jquery-1.12.0.min.js"></script>
<script src="js/myjs.js"></script>
<script type="text/javascript" src="DataTables/datatables.min.js"></script>	
</body>
</html>


<?php
if($uploadedStatus==1)
{

  include 'config.php';

  set_include_path(get_include_path() . PATH_SEPARATOR . 'Classes/');
  include 'PHPExcel/IOFactory.php';
  try
  {
    $objPHPExcel = PHPExcel_IOFactory::load($storagename);
  }
  catch(Exception $e)
  {
    die('Error loading file "'.pathinfo($storagename,PATHINFO_BASENAME).'": '.$e->getMessage());
  }


  $allDataInSheet = $objPHPExcel->getActiveSheet()->toArray(null,true,true,true);
  $arrayCount = count($allDataInSheet);  // Here get total count of row in that Excel sheet
  $i = 2;

  $truncateQuery = "TRUNCATE TABLE dhis.mfl_community_units";
  $truncateResult = mysqli_query($conn,$truncateQuery);
  
  while($i<=$arrayCount){
      
      $values[] = "('".mysqli_real_escape_string($conn, trim($allDataInSheet[$i]["B"]))."', '".mysqli_real_escape_string($conn, trim($allDataInSheet[$i]["A"]))."', 
      	'".mysqli_real_escape_string($conn, trim($allDataInSheet[$i]["D"]))."', '".mysqli_real_escape_string($conn, trim($allDataInSheet[$i]["F"]))."', 
      	'".mysqli_real_escape_string($conn, trim($allDataInSheet[$i]["G"]))."')";
      
      if(($i % 1000) == 0 || $i == $arrayCount){

        $insertStatement = "INSERT INTO mfl_community_units (Name, Code, Facility, SubCounty, County) VALUES ".implode(",",$values);
        //echo "<br><br>".$insertStatement."<br><br>";
        $query = mysqli_query($conn, $insertStatement);

        $values = null;
        if($query)
        {
          $msg = '<br><br>Record has been added. <div style="Padding:20px 0 0 0;"></div>';
          //echo "<br><br>".$msg."<br><br>";
        }
        else
        {
            $msg = '<br><br>An error has occurred. <div style="Padding:20px 0 0 0;"></div>';
            //echo "<br><br>".$msg."<br><br>";
        }
      }
      $i++; 
  }
      

  	//$insert3 = mysqli_query($conn,"DELETE FROM chu_table WHERE Date_established = '0000-00-00'");
    
    echo "<div style='font: bold 18px arial,verdana;padding: 45px 0 0 500px;'>".$msg."</div>";


}
?>