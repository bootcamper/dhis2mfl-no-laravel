<?php

    include 'config.php';

    $query = "SELECT Name,Code,County,Constituency FROM mfl_facilities WHERE mfl_facilities.Code NOT IN (SELECT code from dhis_facilities) AND mfl_facilities.Name
    NOT IN (select name FROM dhis_facilities where code = 'Unassigned')";
	
    $result = mysqli_query($conn,$query);
    $facilities = mysqli_fetch_all($result,MYSQLI_ASSOC);
    echo json_encode($facilities);

?>     
