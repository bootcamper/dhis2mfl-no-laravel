<?php

include 'config.php';

//HTTP GET request -Using Curl -Response JSON

$headers = array(
    'Content-Type: application/json',
);

$ch = curl_init();


curl_setopt($ch, CURLOPT_URL, $levelFiveUrl);
curl_setopt($ch, CURLOPT_POST, false);
curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true );
curl_setopt($ch, CURLOPT_USERPWD, "$username:$password");

$result = curl_exec($ch);
curl_close($ch);

    $truncateQuery = "TRUNCATE TABLE dhis.dhis_community_units";
    $truncateResult = mysqli_query($conn,$truncateQuery);

    if ($result) {
        
        $json = json_decode($result, TRUE);
        $i=0;

        foreach ($json['organisationUnits'] as $val) {
            $i++;
            if(isset($val["code"])){    
                $code = $val["code"];
            }else{
                $code = 'Unassigned'; 
            }
            
                        
            $new[] = "('" . trim($code) . "',  '" . $val["level"] . "', '" . trim(mysqli_real_escape_string($conn,$val["name"])) . "', 
            '" . $val["id"] . "', '" . trim(mysqli_real_escape_string($conn,$val["shortName"])). "', '" .$val["parent"]["id"]. "', 
            '" . trim(mysqli_real_escape_string($conn,$val["parent"]["name"])). "')";

            if(($i % 1000) == 0 || $i == sizeof($json['organisationUnits'])){
                $sqlInsertQuery = "INSERT INTO dhis_community_units(code,level,name,id,shortName,parent_id,parent_name) VALUES ".implode(',', $new);

                $mysqlQuery = mysqli_query($conn,$sqlInsertQuery);
                
                $new = null;

                if($mysqlQuery){
                    echo "Query Successful"."<br><br>";
                }else{
                    echo "Query Unsuccessful"."<br><br>"; 
                }
            }
            
        }

    }              

 ?>