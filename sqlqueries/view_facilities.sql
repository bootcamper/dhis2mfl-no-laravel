CREATE 
    ALGORITHM = UNDEFINED 
    DEFINER = `root`@`localhost` 
    SQL SECURITY DEFINER
VIEW `dhis_facilities` AS
    SELECT 
        `mfl_table`.`id` AS `id`,
        `mfl_table`.`Name` AS `Name`,
        `mfl_table`.`Code` AS `Code`,
        `mfl_table`.`County` AS `County`,
        `mfl_table`.`Constituency` AS `Constituency`
    FROM
        `mfl_table`