CREATE TABLE `levelsix` (
  `id_pk` int(11) NOT NULL AUTO_INCREMENT,
  `code` int(11) NOT NULL,
  `level` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `id` varchar(255) NOT NULL,
  `shortName` varchar(255) NOT NULL,
  PRIMARY KEY (`id_pk`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1;
