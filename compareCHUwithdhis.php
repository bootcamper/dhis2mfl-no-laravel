<?php
     	
        include 'config.php';


        $query = "SELECT Name,Code,Facility, SubCounty FROM mfl_community_units WHERE mfl_community_units.Code NOT IN (SELECT code from dhis_community_units) AND mfl_community_units.Name NOT IN (select name FROM dhis_facilities where code = 'Unassigned') ORDER BY Name";

        $result = mysqli_query($conn,$query);
        $communityHUnits = mysqli_fetch_all($result,MYSQLI_ASSOC);
        echo json_encode($communityHUnits);
?>     
