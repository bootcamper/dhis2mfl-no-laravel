$(document).ready(function(){
//Function to remove the fakepath name from the filename of the 
    $('#fileInput').change(function(){
        var filepath = this.value;
        filepath = filepath.replace("C:\\fakepath\\","");
        $('#fileName').val(filepath);
    });

    //Function to click file uploadButton
    $('#uploadButton').click(function(){
        $('#fileInput').click();
    });

    //Function to load the compare MFL Facilities page
    $('.addFacilities').click(function(){
        $('#pageContent').load('add-mfl.html');
        //populateSubCounties();
        createTable();
    });

    //Function to load the import Facilities page
    $('.facilityImport').click(function(){
        $('#pageContent').load('import-mfl.html');
    });

    //Function to load the import Community Units page
    $('.communityUnitImport').click(function(){
        $('#pageContent').load('import-mcl.html');
    });

    //Function to load the update facilities page
    $('.updateFacilityNames').click(function(){
        $('#pageContent').load('update-facilities.html');
        mflNameComparison();
        mflCodeComparison();
    });

    $('.facilitiesView').click(function(){
        $('#pageContent').load('view_facilities.html');
        createfacilityTable("allmfl");
        createfacilityTable("mfl");
        createfacilityTable("nomfl");
    });

    $('.communityUnitsView').click(function(){
        $('#pageContent').load('view_community_units.html');
        //loadCommunityunits("mcl");
        createCommunityUnitsTable("allmcl");
        createCommunityUnitsTable("mcl");
        createCommunityUnitsTable("nomcl");
    });
	//Function to handle compare button click
	$('#addFacilityButton').click(function(){
		addOrgUnits();
	});
    $('#updatechuNameButton').click(function(){
        updateCommunityUnits();
    });
    
    $('.updateCommunityUnits').click(function(){
        $('#pageContent').load('updateCommunityUnits.html');
        loadCommunityunits();
    });

    $('.viewLevelSix').click(function(){
        $('#pageContent').load('levelsix_track.html');
        createTrackOutliers();
    });

    $('.viewFacilitiesTrack').click(function(){
        $('#pageContent').load('levelfour_track.html');
        createFacilityTrack();
    });

    $('.viewCHUs').click(function(){
        $('#pageContent').load('levelfive_track.html');
        createTrackCHU();
    });

    $('#updateMflNameButton').click(function(){
        updateOrgUnitName();
    });

    $('#updateMflCodeButton').click(function(){
        updateOrgUnitCode();
    })

    $('#subCountySelect').change(function(){
        var selectValue = $('#subCountySelect').val();
        $('#parentIdAdd').val(selectValue);
    });

    $('.updateCommunityUnitsNames').click(function(){
        $('#pageContent').load('updateCommunityUnits.html');
        mclNameComparison();
    });

    $('.addCommunityUnits').click(function(){
        $('#pageContent').load('add-mcl.html');
        //populateSubCounties();
        createmclTable();
    });
    $('#addCommunityUnitButton').click(function(){
        addCommunityHealthUnits();
    });

    //MFL Radio buttons manipulation
    $("input[name=mflRadio]:radio").change(function(){
        var option = $("input:radio[name=mflRadio]:checked").val();

        if(option == "mfl"){
            $('#facilityTable').load('mflcoded.html');
            createfacilityTable(option);
        }else if(option == "nomfl"){
            $('#facilityTable').load('nomflcoded.html');
            createfacilityTable(option);
        }
    });

    $('#countyUpdateMflCodeSelect').change(function(){
        populateMflSubCounties();
    });

    $('#subCountyUpdateMflCodeSelect').change(function(){
        populateMflFacilities();
    });

    $('#facilityMflNameUpdateMflCodeSelect').change(function(){
        var mflCode = $('#facilityMflNameUpdateMflCodeSelect').val();
        $('#newMflCodeUpdate').val(mflCode);
    });

    
    function loadMfl(){
        $('#facilityTable').load('mflcoded.html');
        createfacilityTable("mfl");
    }


    //CommunityUnits Radio buttons manipulation

    $("input[name=mclRadio]:radio").change(function(){
        var option = $("input:radio[name=mclRadio]:checked").val();

        if(option == "mclUnits"){
            $('#communityUnitsTable').load('mflcode_communityunits.html');
            createcommunityUnitsTable(option);
        }else if(option == "nomclUnits"){
            $('#communityUnitsTable').load('nomfl_communityunits.html');
            createcommunityUnitsTable(option);
        }
    });

    
    function loadCommunityunits(){
        $('#communityUnitsTable').load('mflcode_communityunits.html');
        createCommunityUnitsTable("mclUnits");
    }

	//AJAX Function to handle any request made
	function makeRequest(url, callback) {
        var request;

        if (window.XMLHttpRequest) {
            request = new XMLHttpRequest(); // IE7+, Firefox, Chrome, Opera, Safari
        } else {
            request = new ActiveXObject("Microsoft.XMLHTTP"); // IE6, IE5
        }
        request.onreadystatechange = function() {
            if (request.readyState == 4 && request.status == 200) {
                callback(request);
            }
        }
        request.open("GET", url, true);
        request.send();
        
    }

    function populateSubCounties(){

        var mylist = document.getElementById('subCountySelect');
       
        makeRequest('subcountylist.php', function(data){
            var data = JSON.parse(data.responseText);
            var HTML = "<option>Please select a sub county</option>";
            for(var i=0;i<data.length;i++){
                HTML += "<option value="+ data[i].id +">"+data[i].name+"</option>"
            }

            //console.log(HTML);
            mylist.innerHTML = HTML;
        });
    }

    function populateCounties(){

        var mylist = document.getElementById('countyUpdateMflCodeSelect');
        
        makeRequest('countylist.php', function(data){
            var data = JSON.parse(data.responseText);
            var HTML = "";
            for(var i=0;i<data.length;i++){
                HTML += "<option value="+ data[i].id +">"+data[i].name+"</option>"
            }

            //console.log(HTML);
            mylist.innerHTML = HTML;
        });
    }

    function populateMflSubCounties(){

        var mylist = document.getElementById('subCountyUpdateMflCodeSelect');

        var county = $('#countyUpdateMflCodeSelect').val();

        makeRequest('mflsubcountylist.php?county='+county, function(data){
            var data = JSON.parse(data.responseText);
            var HTML = "<option>Please select a sub county</option>";
            for(var i=0;i<data.length;i++){
                HTML += "<option>"+data[i].sub_counties+"</option>"
            }

            //console.log(HTML);
            mylist.innerHTML = HTML;
        });
    }

    function populateMflCounties(){
        var mylist = document.getElementById('countyUpdateMflCodeSelect');
        
        makeRequest('mflcountylist.php', function(data){
            var data = JSON.parse(data.responseText);
            var HTML = "<option>Please select a county</option>";
            for(var i=0;i<data.length;i++){
                HTML += "<option>"+data[i].counties+"</option>"
            }

            //console.log(HTML);
            mylist.innerHTML = HTML;
        });
    }

    function populateMflFacilities(){
        var mylist = document.getElementById('facilityMflNameUpdateMflCodeSelect');

        var subCounty = $('#subCountyUpdateMflCodeSelect').val();
        
        makeRequest('mflfacilitylist.php?subCounty='+subCounty, function(data){
            var data = JSON.parse(data.responseText);
            var HTML = "<option>Please select a facility</option>";
            for(var i=0;i<data.length;i++){
                HTML += "<option value="+ data[i].Code +">"+data[i].Name+"</option>"
            }

            //console.log(HTML);
            mylist.innerHTML = HTML;
        });
    }


    //Function to create the table that shows the data that doesn't match between DHIS and MFL
    function createTable(){

        makeRequest('comparemflwithdhis.php',function(data){//Make AJAX request to database
            var dhisFacilitiesInMfl = JSON.parse(data.responseText);//Put response from database in variable
                if(dhisFacilitiesInMfl.length == 0){
                    alert("There are no facilities that you don't have present in DHIS.");//Message to show that all org units are up to date
                }else{

                    var newTable = $('#facilitiesNotPresent').DataTable({
                        dom: 'Bfrtip',
                        buttons: [
                            {
                                extend:'copyHtml5',
                                title:'Facilities not present in DHIS'
                            },
                            {
                                extend:'excelHtml5',
                                title:'Facilities not present in DHIS'
                            },
                            {
                                extend:'csvHtml5',
                                title:'Facilities not present in DHIS'
                            },
                            {
                                extend:'pdfHtml5',
                                title:'Facilities not present in DHIS'
                            }
                        ],
                        data: dhisFacilitiesInMfl,
                        destroy: true,
                        "pageLength": 10,
                        columns: [
                            { data: "Name" },
                            { data: "Code" },
                            { data: "County"   },
                            { data: "Constituency"}
                        ]
                    });

                $('#facilitiesNotPresent tbody').on('click', 'tr', function () {
                    var data = newTable.row(this).data();

                    $('#mflCodeAdd').val(data.Code);
                    $('#facilityNameAdd').val(data.Name);
                    //console.log("Parent ID =" + data.id)
                    populateSubCounties();
                } );

            }
        });   
    }

    //Create table that shows Community Units 
    function createmclTable(){
        
        //mcl_tableHTML+="<tr><th>Code</th><th>CommunityUnit Name</th><th>Facility Name</th><th>Facility Sub-County</th></tr>"

        makeRequest('compareCHUwithdhis.php',function(data){
            var communityHealthUnits = JSON.parse(data.responseText);

            if(communityHealthUnits.length == 0){
                alert("There are no Community Units  that you don't have present in DHIS.");
            }else{
                var mcl_tableHTML=$('#communityUnitsNotPresent').DataTable({
                    dom: 'Bfrtip',
                        buttons: [
                            {
                                extend:'copyHtml5',
                                title:'Community Units not present in DHIS'
                            },
                            {
                                extend:'excelHtml5',
                                title:'Community Units not present in DHIS'
                            },
                            {
                                extend:'csvHtml5',
                                title:'FCommunity Units not present in DHIS'
                            },
                            {
                                extend:'pdfHtml5',
                                title:'Community Units  not present in DHIS'
                            }
                        ],
                        data: communityHealthUnits,
                        destroy: true,
                        "pageLength": 10,
                        columns: [
                            { data: "Name" },
                            { data: "Code" },
                            { data: "Facility"   },
                            { data: "SubCounty"}
                        ]
                    });
            $('#communityUnitsNotPresent tbody').on('click', 'tr', function () {
                    var data = mcl_tableHTML.row(this).data();

                    $('#mclCodeAdd').val(data.Code);
                    $('#communityUnitNameAdd').val(data.Name);
                    console.log("Parent ID =" + data.id)
                    populateSubCounties();
                } );

            }
        });   
    }

    function createFacilityTrack(){
        makeRequest('levelfour_track.php',function(data){//Make AJAX request to database
            var trackFacilities = JSON.parse(data.responseText);//Put response from database in variable
                if(trackFacilities.length == 0){
                    alert("There are no facilities to be tracked.");//Message to show that all org units are up to date
                }else{
                    console.log("running");
                    $('#facilitiesTrack').DataTable({
                        data: trackFacilities,
                        destroy: true,
                        "pageLength": 10,
                        columns: [
                            { data: "code" },
                            { data: "name" },
                            { data: "id" }
                        ]
                    });

            }
        }); 
    }

    function createTrackOutliers(){

        makeRequest('levelsix_track.php',function(data){//Make AJAX request to database
            var trackOutliers = JSON.parse(data.responseText);//Put response from database in variable
                if(trackOutliers.length == 0){
                    alert("There are no level six facilities to be tracked.");//Message to show that all org units are up to date
                }else{

                    $('#trackLevelOutliers').DataTable({
                        dom: 'Bfrtip',
                        buttons: [
                            {
                                extend:'copyHtml5',
                                title:'Facilities without Community Unit Codes in DHIS'
                            },
                            {
                                extend:'excelHtml5',
                                title:'Facilities without Community Unit Codes in DHIS'
                            },
                            {
                                extend:'csvHtml5',
                                title:'Facilities without Community Unit Codes in DHIS'
                            },
                            {
                                extend:'pdfHtml5',
                                title:'Facilities without Community Unit Codes in DHIS'
                            }
                        ],
                        data: trackOutliers,
                        destroy: true,
                        "pageLength": 10,
                        columns: [
                            { data: "code" },
                            { data: "name" },
                            { data: "id"   },
                            { data: "level"}
                        ]
                    });

            }
        });      
    }

    function createTrackCHU(){

        makeRequest('levelfive_track.php',function(data){//Make AJAX request to database
            var communityUnits = JSON.parse(data.responseText);//Put response from database in variable
                if(communityUnits.length == 0){
                    alert("There are no community units to be investigated.");//Message to show that all org units are up to date
                }else{

                    $('#communityUnitsTrack').DataTable({
                        dom: 'Bfrtip',
                        buttons: [
                            {
                                extend:'copyHtml5',
                                title:'Community Units to be investigated'
                            },
                            {
                                extend:'excelHtml5',
                                title:'Community Units to be investigated'
                            },
                            {
                                extend:'csvHtml5',
                                title:'Community Units to be investigated'
                            },
                            {
                                extend:'pdfHtml5',
                                title:'Community Units to be investigated'
                            }
                        ],
                        data: communityUnits,
                        destroy: true,
                        "pageLength": 10,
                        columns: [
                            { data: "code" },
                            { data: "name" },
                            { data: "id" }
                        ]
                    });

            }
        });   
    }
 
    function createfacilityTable(tableType){
    
        var url;

        if(tableType == "allmfl"){
            url = 'view_facilities.php?type=0';
        }else if(tableType == "mfl"){
            url = 'view_facilities.php?type=1';
        }else if(tableType == "nomfl"){
            url = 'view_facilities.php?type=2';
        }
        
        makeRequest(url,function(data){
            var facilities = JSON.parse(data.responseText);
            //var tableHTML = "";

            if(facilities.length == 0){
                alert("An error occurred somewhere. Please try again later.");
            }else{ 	

	            if(tableType == "allmfl"){
	                //$('tableMflFacilityBody').html = tableHTML;
	                $('#allMfl').DataTable({
	                	dom: 'Bfrtip',
				        buttons: [
                            {
                                extend:'copyHtml5',
                                title:'Facilities with MFL Codes in DHIS'
                            },
                            {
                                extend:'excelHtml5',
                                title:'Facilities with MFL Codes in DHIS'
                            },
                            {
                                extend:'csvHtml5',
                                title:'Facilities with MFL Codes in DHIS'
                            },
                            {
                                extend:'pdfHtml5',
                                title:'Facilities with MFL Codes in DHIS'
                            }
                        ],             	
	                    data: facilities,                 
	                    destroy:true,
	                    "pageLength": 10,
	                    columns: [
	                        { data: "code" },
	                        { data: "name" },
	                        { data: "parent_name" },
                            { data: "county" }
	                    ]
	                });
	            }else if(tableType == "mfl"){
                    //$('tableMflFacilityBody').html = tableHTML;
                    $('#mflCoded').DataTable({
                        dom: 'Bfrtip',
                        buttons: [
                            'copyHtml5',
                            'excelHtml5',
                            'csvHtml5',
                            'pdfHtml5'
                        ],              
                        data: facilities,                 
                        destroy:true,
                        "pageLength": 10,
                        columns: [
                            { data: "code" },
                            { data: "name" },
                            { data: "parent_name" },
                            { data: "county" }
                        ]
                    });
                }else if(tableType == "nomfl"){
	                //$('#tableNoMflFacilityBody').html = tableHTML;
	                $('#noMflCoded').DataTable({
	                	dom: 'Bfrtip',
				        buttons: [
                            {
                                extend:'copyHtml5',
                                title:'Facilities without MFL Codes in DHIS'
                            },
                            {
                                extend:'excelHtml5',
                                title:'Facilities without MFL Codes in DHIS'
                            },
                            {
                                extend:'csvHtml5',
                                title:'Facilities without MFL Codes in DHIS'
                            },
                            {
                                extend:'pdfHtml5',
                                title:'Facilities without MFL Codes in DHIS'
                            }
                        ],
	                    data: facilities,
	                    destroy: true,
	                    "pageLength": 10,
	                    columns: [
	                        { data: "code" },
	                        { data: "name" },
	                        { data: "parent_name" },
                            { data: "county" }
	                    ]
	                });
	            }
            }
        });
    }

    function createCommunityUnitsTable(tableType){

        
        var url;

        if(tableType == "allmcl"){
            url = 'view_community_units.php?type=0';
        }else if(tableType == "mcl"){
            url = 'view_community_units.php?type=1';
        }else if(tableType == "nomcl"){
            url = 'view_community_units.php?type=2';
        }
        
        

        makeRequest(url,function(data){
            var communityUnits = JSON.parse(data.responseText);

            if(communityUnits.length == 0){
                alert("An error occurred somewhere. Please try again later.");
            }else{

                if(tableType == "allmcl"){
                    $('#allmflcode_communityunits').DataTable({
                        dom: 'Bfrtip',
                        buttons: [
                            {
                                extend:'copyHtml5',
                                title:'All Community Units in DHIS'
                            },
                            {
                                extend:'excelHtml5',
                                title:'All Community Units in DHIS'
                            },
                            {
                                extend:'csvHtml5',
                                title:'All Community Units in DHIS'
                            },
                            {
                                extend:'pdfHtml5',
                                title:'All Community Units in DHIS'
                            }
                        ],              
                        data: communityUnits,                 
                        destroy:true,
                        "pageLength": 10,
                        columns: [
                            { data: "code" },
                            { data: "name" },
                            { data: "parent_name" },
                            { data: "sub_county" },
                            {data: "county" }
                        ]
                    });
                }else if(tableType == "mcl"){
                    $('#mflcode_communityunits').DataTable({
                        dom: 'Bfrtip',
                        buttons: [
                            {
                                extend:'copyHtml5',
                                title:'All Community Units in DHIS with community unit codes'
                            },
                            {
                                extend:'excelHtml5',
                                title:'All Community Units in DHIS with community unit codes'
                            },
                            {
                                extend:'csvHtml5',
                                title:'All Community Units in DHIS with community unit codes'
                            },
                            {
                                extend:'pdfHtml5',
                                title:'All Community Units in DHIS with community unit codes'
                            }
                        ],
                        data: communityUnits,
                        destroy:true,
                        "pageLength": 10,
                        columns: [
                            { data: "code" },
                            { data: "name" },
                            { data: "parent_name" },
                            { data: "sub_county" },
                            { data: "county" }
                        ]
                    });
            }else if(tableType == "nomcl"){
                $('#nomfl_communityunits').DataTable({
                    dom: 'Bfrtip',
                    buttons: [
                        {
                            extend:'copyHtml5',
                            title:'Facilities in DHIS without Community Unit Codes'
                        },
                        {
                            extend:'excelHtml5',
                            title:'Facilities in DHIS without Community Unit Codes'
                        },
                        {
                            extend:'csvHtml5',
                            title:'Facilities in DHIS without Community Unit Codes'
                        },
                        {
                            extend:'pdfHtml5',
                            title:'Facilities in DHIS without Community Unit Codes'
                        }
                    ],
                    data: communityUnits,
                    destroy: true,
                    "pageLength": 10,
                    columns: [
                        { data: "code" },
                        { data: "name" },
                        { data: "parent_name" },
                        { data: "sub_county" },
                        { data: "county" }
                    ]
                });
            }
            
            
        }
        });
    }

    function mflNameComparison(){

        makeRequest('getdifforgunit.php',function(data){
            var names = JSON.parse(data.responseText);

            if(names.length == 0){
                alert("An error occurred somewhere. Please try again later.");
            }else{
            
                var table = $('#updateMflNameTable').DataTable({
                    dom: 'Bfrtip',
                    buttons: [
                        {
                            extend:'copyHtml5',
                            title:'Facilities with different names in DHIS and MFL'
                        },
                        {
                            extend:'excelHtml5',
                            title:'Facilities with different names in DHIS and MFL'
                        },
                        {
                            extend:'csvHtml5',
                            title:'Facilities with different names in DHIS and MFL'
                        },
                        {
                            extend:'pdfHtml5',
                            title:'Facilities with different names in DHIS and MFL'
                        }
                    ],
                    data: names,
                    destroy: true,
                    "pageLength": 10,
                    columns: [
                        { data: "code" },
                        { data: "Name" },
                        { data: "dhis" },
                        { data: "id"},
                        { data: "parentname" }
                        
                    ]
                });

                $('#updateMflNameTable tbody').on('click', 'tr', function () {
                    var data = table.row(this).data();

                    $('#currentNameUpdate').val(data.dhis);
                    $('#nameToUpdateTo').val(data.Name);
                    $('#facilityID').val(data.id);

                } );

            }


        });
    }

    function mflCodeComparison(){

        makeRequest('getdiffmflcode.php',function(data){
            var names = JSON.parse(data.responseText);

            if(names.length == 0){
                alert("An error occurred somewhere. Please try again later.");
            }else{
            
                var table = $('#updateMflCodeTable').DataTable({
                    dom: 'Bfrtip',
                    buttons: [
                        {
                            extend:'copyHtml5',
                            title:'Facilities with invalid MFL codes'
                        },
                        {
                            extend:'excelHtml5',
                            title:'Facilities with invalid MFL codes'
                        },
                        {
                            extend:'csvHtml5',
                            title:'Facilities with invalid MFL codes'
                        },
                        {
                            extend:'pdfHtml5',
                            title:'Facilities with invalid MFL codes'
                        }
                    ],
                    data: names,
                    destroy: true,
                    "pageLength": 10,
                    columns: [
                        { data: "code" },
                        { data: "name" },
                        { data: "id"},
                        { data: "parent_name" },
                        { data: "county" },
                        
                    ]
                });

                $('#updateMflCodeTable tbody').on('click', 'tr', function () {
                    var data = table.row(this).data();

                    $('#currentMflCodeUpdate').val(data.code);
                    $('#facilityNameMflCodeUpdate').val(data.name);
                    $('#countyUpdateMflCode').val(data.county);
                    $('#subCountyUpdateMflCode').val(data.parent_name);
                    $('#mflCodeFacilityIdUpdate').val(data.id);
                    $('#newMflCodeUpdate').val("");
                    populateMflCounties();
                    populateMflSubCounties();
                    populateMflFacilities();
                });

            }


        });
    }

    function addOrgUnits(){
        var mflCode = $('#mflCodeAdd').val();
        var facilityName = $('#facilityNameAdd').val();
        var parentID = $('#parentIdAdd').val();

        makeRequest('addorgunit.php?mflCode='+mflCode+'&facilityName='+facilityName+'&parentID='+parentID,function(data){
            var response = JSON.parse(data.responseText);

            if(response.length == 0){
                alert("Sorry, this request couldn't be completed at this time");
                $('#mflCodeAdd').val("");
                $('#facilityNameAdd').val("");
                $('#parentIdAdd').val("");
            }else{
                alert("The organisation unit has been updated");
                $('#mflCodeAdd').val("");
                $('#facilityNameAdd').val("");
                $('#parentIdAdd').val("");
                createTable();
            }



        });
    }

    function updateOrgUnitName(){

        var name = $('#nameToUpdateTo').val();
        var id = $('#facilityID').val();


        makeRequest('updateorgunitname.php?name='+name+'&id='+id,function(data){
            var response = JSON.parse(data.responseText);

            if(response.length == 0){
                alert("Sorry, this request couldn't be completed at this time");
                $('#currentNameUpdate').val("");
                $('#nameToUpdateTo').val("");
                $('#facilityID').val("");
            }else{
                alert("The organisation unit name has been updated");
                $('#currentNameUpdate').val("");
                $('#nameToUpdateTo').val("");
                $('#facilityID').val("");
                mflNameComparison();
            }



        });
    } 

    function updateOrgUnitCode(){

        if($('#newMflCodeUpdate').val() == ""){
            alert("Please select a facility first.");
            return;
        }

        var code = $('#newMflCodeUpdate').val();
        var id = $('#mflCodeFacilityIdUpdate').val();


        makeRequest('updateorgunitcodes.php?code='+code+'&id='+id,function(data){
            var response = JSON.parse(data.responseText);

            if(response.length == 0){
                alert("Sorry, this request couldn't be completed at this time");
                $('#currentMflCodeUpdate').val("");
                $('#facilityNameMflCodeUpdate').val("");
                $('#countyUpdateMflCode').val("");
                $('#subCountyUpdateMflCode').val("");
                $('#countyUpdateMflCodeSelect').html() = "";
                $('#subCountyUpdateMflCodeSelect').html() = "";
                $('#facilityMflNameUpdateMflCodeSelect').html() = "";
            }else{
                alert("The organisation unit code has been updated");
                $('#currentMflCodeUpdate').val("");
                $('#facilityNameMflCodeUpdate').val("");
                $('#countyUpdateMflCode').val("");
                $('#subCountyUpdateMflCode').val("");
                $('#countyUpdateMflCodeSelect').innerHTML = "";
                $('#subCountyUpdateMflCodeSelect').innerHTML = "";
                $('#facilityMflNameUpdateMflCodeSelect').innerHTML = "";

                mflCodeComparison();
            }



        });
    }   

    function rowHandlers(){
        var table = document.getElementById("mfl-table");
        var rows = document.getElementsByTagName("tr");
        for(var i = 1; i<rows.length; i++){
            var currentRow = table.rows[i];
            var ClickHandler = function(row){
                return function(){
                    var mflCodeCell = row.getElementsByTagName("td")[1];
                    var countyCell = row.getElementsByTagName("td")[2];
                    var subCountyCell = row.getElementsByTagName("td")[3];
                    

                    var mflCode = mflCodeCell.innerHTML;
                    var county = countyCell.innerHTML;
                    var subCounty = subCountyCell.innerHTML;

                    $('#mflCode').val(mflCode);
                    $('#county').val(county);
                    $('#subCounty').val(subCounty);
                };
            };

            var doubleClickHandler = function(row){
                return function(){
                    var cell1 = row.getElementsByTagName("td")[0];
                    var cell2 = row.getElementsByTagName("td")[1];
                    var id = cell1.innerHTML;
                    var stageName = cell2.innerHTML;
                    showRouteName(id);
                };
            };

                currentRow.onclick = ClickHandler(currentRow);
                currentRow.ondblclick = doubleClickHandler(currentRow);

                
        }
    }

    function mclNameComparison(){

        makeRequest('getdiffcommunityunits.php',function(data){
            var mclnames = JSON.parse(data.responseText);

            if(mclnames.length == 0){
                alert("An error occurred somewhere. Please try again later.");
            }else{
            
                var mcltable = $('#mclNameUnmatched').DataTable({
                    dom: 'Bfrtip',
                    buttons: [
                        {
                            extend:'copyHtml5',
                            title:'Community Units with different names in DHIS and MFL'
                        },
                        {
                            extend:'excelHtml5',
                            title:'Community Units with different names in DHIS and MFL'
                        },
                        {
                            extend:'csvHtml5',
                            title:'Community Units with different names in DHIS and MFL'
                        },
                        {
                            extend:'pdfHtml5',
                            title:'Community Units with different names in DHIS and MFL'
                        }
                    ],
                    data: mclnames,
                    destroy: true,
                    "pageLength": 10,
                    columns: [
                        { data: "code" },
                        { data: "Name" },
                        { data: "dhis" },
                        { data: "id"},
                        { data: "parentname" }
                        
                    ]
                });

                $('#mclNameUnmatched tbody').on('click', 'tr', function () {
                    var data = mcltable.row(this).data();

                    $('#currentmclNameUpdate').val(data.dhis);
                    $('#mclnameToUpdateTo').val(data.Name);
                    $('#communityUnitID').val(data.id);

                } );

            }


        });
    }

    function updateCommunityUnits(){

        var name = $('#mclnameToUpdateTo').val();
        var id = $('#communityUnitID').val();


        makeRequest('updatecomm-unitname.php?name='+name+'&id='+id,function(data){
            var response = JSON.parse(data.responseText);

            if(response.length == 0){
                alert("Sorry, this request couldn't be completed at this time");
                $('#currentmclNameUpdate').val("");
                $('#mclnameToUpdateTo').val("");
                $('#communityUnitID').val("");
            }else{
                alert("The Community Unit has been updated");
                $('#currentmclNameUpdate').val("");
                $('#mclnameToUpdateTo').val("");
                $('#communityUnitID').val("");
                mclNameComparison();
            }



        });
    } 

    function addCommunityHealthUnits(){
        var mclCode = $('#mclCodeAdd').val();
        var communityUnitsName = $('#communityUnitsNameAdd').val();
        var parentID = $('#parentIdAdd').val();

        makeRequest('addmclunits.php?mclCode='+mclCode+'&communityUnitsName='+communityUnitsName+'&parentID='+parentID,function(data){
            var response = JSON.parse(data.responseText);

            if(response.length == 0){
                alert("Sorry, this request couldn't be completed at this time");
                $('#mclCodeAdd').val("");
                $('#communityUnitsNameAdd').val("");
                $('#parentIdAdd').val("");
            }else{
                alert("The Community Unit has been Added/updated");
                $('#mclCodeAdd').val("");
                $('#communityUnitsNameAdd').val("");
                $('#parentIdAdd').val("");
                createmclTable();
            }



        });
    }  

});