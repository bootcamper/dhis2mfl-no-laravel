<?php

include 'config.php';
//if($con){echo ("hey");}
$query="SELECT Name,Code,County FROM mfl_facilities";

$query="SELECT Name,Code,KephLevel,County FROM mfl_facilities";
$result=mysqli_query($conn,$query);
?>
<!DOCTYPE html>
<html>
	<head>
		<title>My DataTable</title>
		<script type="text/javascript" charset="utf8" src="datatables/media/js/jquery.js"></script>
		<script type="text/javascript" charset="utf8" src="datatables/media/js/jquery.dataTables.js"></script>
		<link rel="stylesheet" href="datatables/media/css/jquery.dataTables.css">
		<script src="//cdn.datatables.net/buttons/1.1.2/js/buttons.print.min.js"></script>
		<script src="//cdn.datatables.net/buttons/1.1.2/js/buttons.html5.min.js"></script>
		<script src="//cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/vfs_fonts.js"></script>
		<script src="//cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/pdfmake.min.js"></script>
		<script src="//cdnjs.cloudflare.com/ajax/libs/jszip/2.5.0/jszip.min.js"></script>
		<!-- <script src="//cdn.datatables.net/buttons/1.1.2/js/buttons.flash.min.js"></script> -->
		<script src="https://cdn.datatables.net/buttons/1.1.2/js/dataTables.buttons.min.js"></script>
		<script src="https://cdn.datatables.net/1.10.11/js/jquery.dataTables.min.js"></script>
		<script type="text/javascript" charset="utf-8">

		$(document).ready(function(){
			$('#datatables').DataTable({
			 dom: 'Bfrtip',
			 buttons: [
			 'copy', 'csv', 'excel', 'pdf', 'print'
			 ]});
		});
		</script>
	</head>
	<body>
	<div>
		<table id="datatables" class="display">	
			<thead>
				<tr>
					<th> Name </th>
					<th> Code </th>
					<th> County </th>
				</tr>
			</thead>
			<tbody>
				<?php 
				while ($row=mysqli_fetch_array($result)) {
					?>
					<tr>
						<td><?=$row['Name']?></td>
						<td><?=$row['Code']?></td>
						<td><?=$row['County']?></td>
					</tr>
				<?php
				}
				?>
			</tbody>
		</table>
	</body>
	</div>
</html>