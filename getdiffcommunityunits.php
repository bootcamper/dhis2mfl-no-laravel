<?php
     	
        include 'config.php';

        $query = "SELECT mfl_community_units.code,mfl_community_units.Name,dhis_community_units.Name AS dhis,dhis_community_units.parent_name AS parentname,dhis_community_units.id AS id FROM dhis.dhis_community_units INNER JOIN dhis.mfl_community_units ON dhis_community_units.Code=mfl_community_units.code WHERE dhis_community_units.name != mfl_community_units.Name";
        
        $result = mysqli_query($conn,$query);
        $dhismflnames = mysqli_fetch_all($result,MYSQLI_ASSOC);
        echo json_encode($dhismflnames);

?>  
