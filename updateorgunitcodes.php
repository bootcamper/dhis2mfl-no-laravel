<?php
/* Represents organisation unit details in json form and updates them in dhis2 in the approporiate hierarchy*/
	include 'config.php';
	include 'addfac.php';

	$code = mysqli_real_escape_string($conn,$_GET['code']);
	$id = mysqli_real_escape_string($conn,$_GET['id']);
	
	$orgUnitName = array("code"=>$code);
	$jsonName = json_encode($orgUnitName);

	//Executes the curl call to update data via the API

	//Concatenate the ID and code to the update URL
	$codeUpdateUrl .= $id."/code";
	//echo $codeUpdateUrl;
	
	
	$ch = curl_init($codeUpdateUrl);
    curl_setopt($ch, CURLOPT_HTTPHEADER,array('Content-Type: application/json'));
    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "PATCH");
	curl_setopt($ch, CURLOPT_POSTFIELDS,$jsonName);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);                                                                      
	curl_setopt($ch, CURLOPT_USERPWD, "$username:$password");
	$result = curl_exec($ch);

	$refreshFacility = addFacilities();

	echo $result;



	
?>