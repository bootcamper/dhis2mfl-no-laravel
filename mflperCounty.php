<?php

    include 'config.php';
        $query = "SELECT dhis_sub_counties.parent_name as county, count(*) FROM dhis.dhis_facilities inner join dhis.dhis_sub_counties on dhis_facilities.parent_id=dhis_sub_counties.id WHERE dhis_facilities.code = 'Unassigned' GROUP by county";
        $result = mysqli_query($conn,$query);
        $perCounty =  mysqli_fetch_all($result,MYSQLI_ASSOC);
        $facilities =json_encode($perCounty);

?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html>

    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        <title>Facilities without MFL per County</title>
        <link rel="stylesheet" href="amcharts/style.css" type="text/css">
        <script src="amcharts/amcharts/amcharts.js" type="text/javascript"></script>
        <script src="amcharts/amcharts/pie.js" type="text/javascript"></script>

        <script>
            var chart;
            var legend;

            var chartData = <?php echo $facilities; echo ";"?>
            AmCharts.ready(function () {
                // PIE CHART
                chart = new AmCharts.AmPieChart();
                chart.dataProvider = chartData;
                chart.titleField = "county";
                chart.valueField = "count(*)";
                chart.outlineColor = "#FFFFFF";
                chart.outlineAlpha = 0.8;
                chart.outlineThickness = 2;
                chart.balloonText = "[[title]]<br><span style='font-size:14px'><b>[[value]]</b> ([[percents]]%)</span>";
                // this makes the chart 3D
                chart.depth3D = 15;
                chart.angle = 30;

                // WRITE
                chart.write("chartdiv");
            });
        </script>
    </head>

    <body>
        <div id="chartdiv" style="width: 100%; height: 400px;"></div>
    </body>

</html>