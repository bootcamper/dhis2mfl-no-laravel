<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>DHIS - MFL</title>
	<link href="css/mycss.css" rel="stylesheet">
	<link rel="stylesheet" href="css/normalize.css">
    <link rel="stylesheet" href="font-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" type="text/css" href="DataTables/datatables.min.css"/>

</head>

<body>

<nav class="navbar navbar-inverse">
	<div class="container-fluid">

		<div class="navbar-header">
	      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
	        <span class="sr-only">Toggle navigation</span>
	        <span class="icon-bar"></span>
	        <span class="icon-bar"></span>
	        <span class="icon-bar"></span>
	      </button>
	      <a class="navbar-brand" href="index.php">DHIS - MFL Tool</a>
	    </div>

		<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
	      	<ul class="nav navbar-nav">
				
				<!-- Dropdown list -->

	        	<li class="dropdown">
	        		<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Facilities<span class="caret"></span></a>
			            <ul class="dropdown-menu">
			            	<li><a href="#" class="updateFacilityNames">Update Facilities</a></li>			    			            	
				            <li><a href="#" class="addFacilities">Add Facilities</a></li>
				            <li><a href="#" class="facilitiesView">View Facilities</a></li>				           				           
				            <li role="separator" class="divider"></li>
				            <li><a href="#">Remove Facilities</a></li>
			            </ul>
	        	</li>

	        	<li class="dropdown">
	        		<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Community Health Units<span class="caret"></span></a>
			            <ul class="dropdown-menu">
			            	<li><a href="#" class="updateCommunityUnitsNames">Update Community Health Units</a></li>
				            <li><a href="#" class="addCommunityUnits">Add Community Health Units</a></li>
   				            <li><a href="#" class="communityUnitsView">View Community Health Units</a></li>				          
				            <li role="separator" class="divider"></li>
				            <li><a href="#">Remove Community Health Units</a></li>
			            </ul>
	        	</li>
				<li class="dropdown">
	        		<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Import<span class="caret"></span></a>
			            <ul class="dropdown-menu">
			            	<li><a href="import_excel.php" >Import Facilites</a></li>				          				          
				            <li role="separator" class="divider"></li>
				            <li><a href="importmcl_excel.php">Import Community Health Units</a></li>
			            </ul>
	        	</li>
	        	<li class="dropdown">
	        		<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Track Facilities<span class="caret"></span></a>
			            <ul class="dropdown-menu">
			            	<li><a href="#" class="viewCHUs">Community Units to be Tracked</a></li>
				            <li><a href="#" class="viewFacilitiesTrack">Facilities to be Tracked</a></li>
				            <li><a href="#" class="viewLevelSix">Facilities in the Wrong Hierarchy Level</a></li>
			            </ul>
	        	</li>

	        	<li><a href="docs\_build\html\index.html">Documentation</a></li>
	        	<li><a href="newdash.php">Dashboard</a></li>
	        	
	      	</ul>

	    </div>
	</div>
</nav>


<div id="pageContent">

	<div class="container-fluid">
		<div class="jumbotron">
			<h2>Welcome to the DHIS2 - MFL Integration Tool</h2>
			<p>This is a tool that will help in minimizing the disparities between the DHIS2 facilities and community units in Kenya and those listed in the Master Facility List.</p>
			<p><a class="btn btn-primary btn-lg" role="button" data-toggle="collapse" href="#collapseExample" 
			aria-expanded="false" aria-controls="collapseExample">
			Learn more
			</a></p>
			<div class="collapse" id="collapseExample">
				<div class="well">
					<p>Please refer to our <a href="docs/_build/html/index.html">Documentation</a> for more information</p>
				</div>
			</div>
		</div>


		<section>
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="section-heading">Features</h1>
                    <div class="row">
                      <div class="col-lg-4">
                          <img class="img-circle img-responsive img-center shadow" src="images/mflzero.png" alt="">
                          <h2>Invalid MFL Codes</h2>
                          <p>This tool helps to find facilities that may have been entered in DHIS without MFL codes or non-existent facilities in the MFL list.</p>
                      </div>
                      <div class="col-lg-4">
                          <img class="img-circle img-responsive img-center shadow" src="images/quicksearch.png" alt="">
                          <h2>Easy Search</h2>
                          <p>Easy search functionality to help search through thousands of records in the DHIS database for the specific record one is searching for.</p>
                      </div>
                      <div class="col-lg-4">
                          <img class="img-circle img-responsive img-center shadow" src="images/import.png" alt="">
                          <h2>One-click Import</h2>
                          <p>Easily import facilities and Community Units from Excel sheets offered on the MFL website into your database so as to have the latest 
                          facilities and community units.</p>
                      </div>
                  </div>
                  
                </div>
            </div>
            <!-- /.row -->
			
			<br><br><br><br><br>

            <div class="row">
                <div class="col-lg-12">
                    
                    <div class="row">
                      <div class="col-lg-4">
                          <img class="img-circle img-responsive img-center shadow" src="images/comparison.png" alt="">
                          <h2>Easy Comparison</h2>
                          <p>Clear and concise comparison between the names of facilities and community units in DHIS and MFL.</p>
                      </div>
                      <div class="col-lg-4">
                          <img class="img-circle img-responsive img-center shadow" src="images/facilityupdate.png" alt="">
                          <h2>Easy Updates to DHIS facility names</h2>
                          <p>Easily update facility names in the DHIS database through fast API calls.</p>
                      </div>
                      	
                  </div>
                  <hr>
                </div>
            </div>
            <!-- /.row -->
        </div>
        <!-- /.container -->
    </section>
	</div>

</div>


<script src="js/jquery-1.12.0.min.js"></script>
<script src="js/myjs.js"></script>
<script type="text/javascript" src="DataTables/datatables.min.js"></script>
</body>
</html>