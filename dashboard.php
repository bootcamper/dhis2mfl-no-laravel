<?php

	include 'config.php';

	//Queries
	$noMflFacilitiesQuery = "SELECT * FROM dhis_facilities WHERE code = 'Unassigned' OR length(code) != 5";
	$mflFacilitiesQuery = "SELECT * FROM dhis_facilities WHERE code != 'Unassigned' AND length(code) = 5";
	$noMculFacilitiesQuery = "SELECT * FROM dhis_community_units WHERE code = 'Unassigned' OR length(code) != 6";
	$mculFacilitiesQuery = "SELECT * FROM dhis_community_units WHERE code != 'Unassigned' AND length(code) = 6";


	//Query Calls
	$noMflFacilitiesResult = mysqli_query($conn,$noMflFacilitiesQuery);
	$mflFacilitiesResult = mysqli_query($conn,$mflFacilitiesQuery);
	$noMculFacilitiesResult = mysqli_query($conn,$noMculFacilitiesQuery);
	$mculFacilitiesResult = mysqli_query($conn,$mculFacilitiesQuery);


	$totalMflCodes = mysqli_num_rows($mflFacilitiesResult);
	$totalNoMflCodes = mysqli_num_rows($noMflFacilitiesResult);

	$totalMculCodes = mysqli_num_rows($mculFacilitiesResult);
	$totalNoMculCodes = mysqli_num_rows($noMculFacilitiesResult); 

?>


<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Dashboard</title>
	<link href="css/mycss.css" rel="stylesheet">
	<link rel="stylesheet" href="css/normalize.css">
    <link rel="stylesheet" href="font-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" type="text/css" href="DataTables/datatables.min.css"/>
    <link rel="stylesheet" type="text/css" href="Chartist/chartist.min.css">
    <script type="text/javascript" src="Chartist/chartist.min.js"></script>

</head>

<body>

	<nav class="navbar navbar-inverse">
		<div class="container-fluid">

			<div class="navbar-header">
		      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
		        <span class="sr-only">Toggle navigation</span>
		        <span class="icon-bar"></span>
		        <span class="icon-bar"></span>
		        <span class="icon-bar"></span>
		      </button>
		      <a class="navbar-brand" href="index.php">DHIS - MFL Tool</a>
		    </div>

			<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
		      	<ul class="nav navbar-nav">
					
					<!-- Dropdown list -->

		        	<li class="dropdown">
		        		<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Facilities<span class="caret"></span></a>
				            <ul class="dropdown-menu">
				            	<li><a href="#" class="updateFacilityNames">Update Facilities</a></li>			    			            	
					            <li><a href="#" class="compareFacilities">Add Facilities</a></li>
					            <li><a href="#" class="facilitiesView">View Facilities</a></li>				           				           
					            <li role="separator" class="divider"></li>
					            <li><a href="#">Remove Facilities</a></li>
				            </ul>
		        	</li>

		        	<li class="dropdown">
		        		<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Community Health Units<span class="caret"></span></a>
				            <ul class="dropdown-menu">
				            	<li><a href="#">Update Community Health Units</a></li>
					            <li><a href="#" class="compareCommunityUnits">Compare Community Health Units</a></li>
					            <li><a href="#" class="communityUnitsView">View Community Health Units</a></li>				          
					            <li role="separator" class="divider"></li>
					            <li><a href="#">Remove Community Health Units</a></li>
				            </ul>
		        	</li>
					<li class="dropdown">
		        		<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Import<span class="caret"></span></a>
				            <ul class="dropdown-menu">
				            	<li><a href="import_excel.php" >Import Facilites</a></li>				          				          
					            <li role="separator" class="divider"></li>
					            <li><a href="importmcl_excel.php">Import Community Health Units</a></li>
				            </ul>
		        	</li>
		        	<li class="dropdown">
		        		<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Track Facilities<span class="caret"></span></a>
				            <ul class="dropdown-menu">
				            	<li><a href="#" class="viewCHUs">Community Units to be Tracked</a></li>
					            <li><a href="#" class="viewFacilitiesTrack">Facilities to be Tracked</a></li>
					            <li><a href="#" class="viewLevelSix">Facilities in the Wrong Hierarchy Level</a></li>
				            </ul>
		        	</li>
		        	<li><a href="#">Documentation</a></li>
		        	<li><a href="#">Help</a></li>
		      	</ul>

		    </div>
		</div>
	</nav>
	

	<div class="container-fluid">
		<div class="row">
			<div class="col-sm-6 col-sm-offset-1">
				<div class="ct-chart ct-golden-section" id="chart1"></div>
			</div>

			

			<div class="col-sm-5">
				<div class="ct-chart ct-golden-section" id="chart2"></div>
			</div>
		
		<div class="row">
			<div class="col-sm-5col-sm-offset-1">
				<div>MFL Facilities with codes: <span><?php echo $totalMflCodes ?></span></div>
				<div>MFL Facilities with codes: <span><?php echo $totalNoMflCodes ?></span></div>
			</div>
			
		</div>

		</div>
	</div>
	
	


	<script>
		var dataMfl = {
			labels: ['Valid MFL Codes','Invalid MFL codes'],
			series: [<?php echo $totalMflCodes.",".$totalNoMflCodes; ?>]
		};

		var dataMcul = {
			labels: ['Valid MCUL Codes','Invalid MCUL codes'],
			series: [<?php echo $totalMculCodes.",".$totalNoMculCodes; ?>]
		};

		var options = {
			width: 300,
			height: 200
		};
	

		new Chartist.Pie('#chart1',dataMfl,{
		  chartPadding: 50,
		  labelOffset: 10,
		  labelDirection: 'explode'
		});

		new Chartist.Pie('#chart2',dataMcul,{
		  chartPadding: 50,
		  labelOffset: 10,
		  labelDirection: 'explode'
		});


	</script>
	<script type="text/javascript" src="js/jquery-1.12.0.min.js"></script>
	<script type="text/javascript" src="js/myjs.js"></script>
	<script type="text/javascript" src="DataTables/datatables.min.js"></script>
	<!-- <script type="text/javascript" src="Chartist/chartist.min.js"></script> -->
</body>
</html>