<?php


include 'config.php';

$subCounty = trim(mysqli_real_escape_string($conn,$_GET['subCounty']));

$query = "SELECT Name,Code FROM dhis.mfl_facilities WHERE Constituency = '$subCounty' ORDER BY Name ASC";

$result = mysqli_query($conn,$query);
$subCounties = mysqli_fetch_all($result,MYSQLI_ASSOC);
echo json_encode($subCounties);

?>