<?php

    include 'config.php';

        $facilityQuery = "SELECT * FROM dhis_facilities WHERE code = 'Unassigned'";

        $result = mysqli_query($conn,$facilityQuery);
        $facilitiesCode = mysqli_num_rows($result);
        $communityQuery = "SELECT * FROM dhis_community_units WHERE code = 'Unassigned' OR length(dhis_community_units.code) != 6";
        $cuResult = mysqli_query($conn, $communityQuery);
        $communityCode = mysqli_num_rows($cuResult);
        $compareQuery = "SELECT Name,Code,County,Constituency FROM mfl_facilities WHERE mfl_facilities.Code NOT IN (SELECT code from dhis_facilities) AND mfl_facilities.Name NOT IN (select name FROM dhis_facilities where code = 'Unassigned')";
        $compaResult = mysqli_query($conn, $compareQuery);
        $compFacilities = mysqli_num_rows($compaResult);

        $compareCommunityUnits = "SELECT Name,Code,Facility, SubCounty FROM mfl_community_units WHERE mfl_community_units.Code NOT IN (SELECT code from dhis_community_units) AND mfl_community_units.Name NOT IN (select name FROM dhis_facilities where code = 'Unassigned') ORDER BY Name";
        $compaCommunity = mysqli_query($conn, $compareCommunityUnits);
        $compCommunityResults = mysqli_num_rows($compaCommunity);
        $invalid = "SELECT name, code, id, level FROM dhis_invalid_facilities";
        $retrieve = mysqli_query($conn, $invalid);
        $invalidFacilities = mysqli_num_rows($retrieve);

?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html>

    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        <title>Overall Reports</title>
        <link rel="stylesheet" href="amcharts/style.css" type="text/css">
        <script src="amcharts/amcharts/amcharts.js" type="text/javascript"></script>
        <script src="amcharts/amcharts/serial.js" type="text/javascript"></script>
        <script>
            var chart;

            var chartData = [
                {
                    "Variable": "Facilities Without MFL Codes",
                    "numbers": <?php echo $facilitiesCode; ?>
                },
                {
                    "Variable": "Community Units without Codes",
                    "numbers": <?php echo $communityCode; ?>
                },
                {
                    "Variable": "Facilities Not in DHIS",
                    "numbers": <?php echo $compFacilities; ?>
                },
                {
                    "Variable": "Community Units Not in DHIS",
                    "numbers": <?php echo $compCommunityResults;?>
                },
                // {
                //     "Variable": "Facilites and Community Units in Wrong Hierarchy",
                //     "numbers": <?php echo $invalidFacilities;?>
                // }
            ];


            AmCharts.ready(function () {
                // SERIAL CHART
                chart = new AmCharts.AmSerialChart();
                chart.dataProvider = chartData;
                chart.categoryField = "Variable";
                chart.startDuration = 1;
                chart.plotAreaBorderColor = "#DADADA";
                chart.plotAreaBorderAlpha = 1;
                // this single line makes the chart a bar chart
                chart.rotate = false;

                // AXES
                // Category
                var categoryAxis = chart.categoryAxis;
                categoryAxis.gridPosition = "start";
                categoryAxis.gridAlpha = 0.1;
                categoryAxis.axisAlpha = 0;

                // Value
                var valueAxis = new AmCharts.ValueAxis();
                valueAxis.axisAlpha = 0;
                valueAxis.gridAlpha = 0.1;
                valueAxis.position = "top";
                chart.addValueAxis(valueAxis);

                // GRAPHS
                var graph2 = new AmCharts.AmGraph();
                graph2.type = "column";
                graph2.title = "Numbers";
                graph2.valueField = "numbers";
                graph2.balloonText = "Numbers:[[value]]";
                graph2.lineAlpha = 0;
                graph2.fillColors = "#81acd9";
                graph2.fillAlphas = 1;
                graph2.topRadius = 2;
                chart.addGraph(graph2);


                // LEGEND
                var legend = new AmCharts.AmLegend();
                chart.addLegend(legend);

                chart.creditsPosition = "top-right";

                // WRITE
                chart.write("chartdiv");
            });
        </script>
    </head>

    <body>
        
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-offset-2" id="chartdiv" style="width:1100px; height:700px;"></div> 
            </div>
        </div>
    </body>

</html>