<?php

//include 'config.php';

?>

<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>DHIS - MFL</title>
	<link href="css/mycss.css" rel="stylesheet">
	<link rel="stylesheet" href="css/normalize.css">
    <link rel="stylesheet" href="font-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" type="text/css" href="DataTables/datatables.min.css"/>

</head>

<body>

<nav class="navbar navbar-inverse">
	<div class="container-fluid">

		<div class="navbar-header">
	      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
	        <span class="sr-only">Toggle navigation</span>
	        <span class="icon-bar"></span>
	        <span class="icon-bar"></span>
	        <span class="icon-bar"></span>
	      </button>
	      <a class="navbar-brand" href="index.php">DHIS - MFL Tool</a>
	    </div>

		<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
	      	<ul class="nav navbar-nav">
				
				<!-- Dropdown list -->

	        	<li class="dropdown">
	        		<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Facilities<span class="caret"></span></a>
			            <ul class="dropdown-menu">
			            	<li><a href="#" class="updateFacilityNames">Update Facilities</a></li>			    			            	
				            <li><a href="#" class="addFacilities">Add Facilities</a></li>
				            <li><a href="#" class="facilitiesView">View Facilities</a></li>				           				           
				            <li role="separator" class="divider"></li>
				            <li><a href="#">Remove Facilities</a></li>
			            </ul>
	        	</li>

	        	<li class="dropdown">
	        		<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Community Health Units<span class="caret"></span></a>
			            <ul class="dropdown-menu">
			            	<li><a href="#">Update Community Health Units</a></li>
				            <li><a href="#" class="compareCommunityUnits">Add Community Health Units</a></li>
				            <li><a href="#" class="communityUnitsView">View Community Health Units</a></li>				          
				            <li role="separator" class="divider"></li>
				            <li><a href="#">Remove Community Health Units</a></li>
			            </ul>
	        	</li>
				<li class="dropdown">
	        		<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Import<span class="caret"></span></a>
			            <ul class="dropdown-menu">
			            	<li><a href="import_excel.php" >Import Facilites</a></li>				          				          
				            <li role="separator" class="divider"></li>
				            <li><a href="importmcl_excel.php">Import Community Health Units</a></li>
			            </ul>
	        	</li>
	        	<li class="dropdown">
	        		<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Track Facilities<span class="caret"></span></a>
			            <ul class="dropdown-menu">
			            	<li><a href="#" class="viewCHUs">Community Units to be Tracked</a></li>
				            <li><a href="#" class="viewFacilitiesTrack">Facilities to be Tracked</a></li>
				            <li><a href="#" class="viewLevelSix">Facilities in the Wrong Hierarchy Level</a></li>
			            </ul>
	        	</li>
	        	<li><a href="docs\_build\html\index.html">Documentation</a></li>
	        	<li><a href="newdash.php">Dashboard</a></li>
	      	</ul>

	    </div>
	</div>
</nav>


<div>
	
	<?php require 'viewmflCodes_piechart.php'; ?>
	
</div>


<div>
	
	<?php require '3D_report.php'; ?>

</div>



<div>
	
	

</div>

</body>

</html>