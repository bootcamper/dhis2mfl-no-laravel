<?php

    include 'config.php';
        $query1 = "SELECT * FROM dhis_facilities WHERE code != 'Unassigned'";
        $query2 = "SELECT * FROM dhis_facilities WHERE code = 'Unassigned'";
        $result1 = mysqli_query($conn,$query1);
        $result2=mysqli_query($conn,$query2);
        $facilitiesCode = mysqli_num_rows($result1);
        $facilitiesNoCode = mysqli_num_rows($result2);

?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html>

    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        <title>Facilities summary</title>
        <link rel="stylesheet" href="amcharts/style.css" type="text/css">
        <script src="amcharts/amcharts/amcharts.js" type="text/javascript"></script>
        <script src="amcharts/amcharts/pie.js" type="text/javascript"></script>

        <script>
            var chart2;

            var chartData2 = [{
                "Facilities": "Facilities with MFL Codes",
                "Number": <?php echo $facilitiesCode;?>,
                "pattern": {
                    "url": "patterns/black/pattern1.png",
                    "width": 4,
                    "height": 4,
                    "color": "#cc0000"
                }
            }, {
                "Facilities": "Facilities without MFL Codes",
                "Number": <?php echo $facilitiesNoCode;?>,
                "pattern": {
                    "url": "patterns/black/pattern2.png",
                    "width": 4,
                    "height": 4
                }
            }];


            AmCharts.ready(function() {
                // PIE CHART
                chart2 = new AmCharts.AmPieChart();

                chart2.dataProvider = chartData2;
                chart2.titleField = "Facilities";
                chart2.valueField = "Number";
                chart2.patternField = "pattern";
                chart2.outlineColor = "#000000";
                chart2.outlineAlpha = 0.6;
                chart2.balloonText = "[[title]]<br><span style='font-size:14px'><b>[[value]]</b> ([[percents]]%)</span>";

                var legend2 = new AmCharts.AmLegend();
                legend2.markerBorderColor = "#000000";
                legend2.switchType = undefined;
                legend2.align = "center";
                chart2.addLegend(legend2);

                // WRITE
                chart2.write("chartdiv2");
            });
        </script>
    </head>

    <body>
        <div class="container-fluid">
            <div class="row">
                <div id="chartdiv2" style="width: 100%; height: 400px;"></div> 
            </div>
        </div>
            
    </body>

</html>