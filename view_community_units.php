<?php

	include 'config.php';

	if($_GET['type'] == 0){
		$query = "SELECT dhis_community_units.code,dhis_community_units.name,dhis_community_units.parent_name, 
dhis.dhis_facilities.parent_name as sub_county,dhis.dhis_sub_counties.parent_name as county FROM 
dhis.dhis_community_units INNER JOIN dhis.dhis_facilities on dhis_community_units.parent_id=dhis_facilities.id 
INNER JOIN dhis.dhis_sub_counties on dhis_facilities.parent_id=dhis_sub_counties.id";

	}else if($_GET['type'] == 1){
		$query = "SELECT dhis_community_units.code,dhis_community_units.name,dhis_community_units.parent_name, 
dhis.dhis_facilities.parent_name as sub_county,dhis.dhis_sub_counties.parent_name as county FROM 
dhis.dhis_community_units INNER JOIN dhis.dhis_facilities on dhis_community_units.parent_id=dhis_facilities.id 
INNER JOIN dhis.dhis_sub_counties on dhis_facilities.parent_id=dhis_sub_counties.id WHERE dhis_community_units.code != 'Unassigned' AND length(dhis_community_units.code) = 6";

	}else if($_GET['type'] == 2){
		$query = "SELECT dhis_community_units.code,dhis_community_units.name,dhis_community_units.parent_name, 
dhis.dhis_facilities.parent_name as sub_county,dhis.dhis_sub_counties.parent_name as county FROM 
dhis.dhis_community_units INNER JOIN dhis.dhis_facilities on dhis_community_units.parent_id=dhis_facilities.id 
INNER JOIN dhis.dhis_sub_counties on dhis_facilities.parent_id=dhis_sub_counties.id WHERE dhis_community_units.code = 'Unassigned' OR length(dhis_community_units.code) != 6";
	}	

	$result = mysqli_query($conn,$query);
	$communityUnits = mysqli_fetch_all($result,MYSQLI_ASSOC);
    echo json_encode($communityUnits);

?>