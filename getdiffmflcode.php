<?php

	include 'config.php';

	$query = "SELECT dhis_facilities.code,dhis_facilities.name,dhis_facilities.id,dhis_facilities.parent_name,dhis_sub_counties.parent_name AS county
	FROM dhis_facilities 
	INNER JOIN dhis_sub_counties ON dhis_facilities.parent_id=dhis_sub_counties.id
	WHERE dhis_facilities.code = 'Unassigned' OR length(dhis_facilities.code) <> 5";

	$result = mysqli_query($conn,$query);
	$invalidCodes = mysqli_fetch_all($result,MYSQLI_ASSOC);
	echo json_encode($invalidCodes);

?>