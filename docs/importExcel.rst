Importing Excel Files
=======================

Currently, the MFL API is not yet available for public use. Therefore, for one to get the latest Master Facility and Community Units Lists, one has to download from MFL's website.

Here's the link for the MFL facilities: https://mfl-public.slade360.co.ke/#/facility_filter/results

Here's the link for the Community Health Units: https://mfl-public.slade360.co.ke/#/chul_filter/results 

.. image:: /image/ImportExcel.png
