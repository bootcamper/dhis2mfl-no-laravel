Updating Name or MFL Code
===========================
The Integration tool allows the users to view facilities in DHIS that differ with those in MFL based on their names or MFL code and also update these facilities with the name or MFL code provided by the Master Facility Listing to ensure the facilities in DHIS and MFL have the same mfl-code and name.

Update Facilty Name
---------------------
This feature allows facilities that have the same MFL code in DHIS and MFL but with different names to be updated with the name provided by the Master Facility List.

.. image:: /image/update.png 

Update MFL Code
-----------------
This feature allows facilities that have the same names in DHIS and MFL but with different MFL codes to be updated with the MFL code provided by the Master Facility List.