Print Reports
==============

These feature allows the reports on the system to be printed or saved on the user's computer for record keeping or use for later perusal.

.. image:: /image/add.png 

#. COPY

	The COPY button on click will automatically trigger the copy function which copies the data the user wants to the clipboard. The user can later paste the data on a word document or excel worksheet.
	
	.. image:: /image/copy.png 


#. Excel

	The EXCEL button on click will automatically triggers the export function which converts the data the user wants to excel format and is saved on the users computer.
#. CSV

	The CSV button on click will automatically trigger the export function which converts the data the user wants to CSV format and the file saved on the users computer.

#. PDF

	The PDF button on click will automatically trigger the export function which converts the data the user wants to pdf. The file is saved on the user's computer and on can also print the file if needed.