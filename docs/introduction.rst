Getting Started
================
Introduction
-------------
MFL also known as the Master Facility List is a Health Information System that contains all the facilities in Kenya. The system maps the facilities geographically and identifies them with their unique GeoCodes.

On the other hand, the District Health Information System (DHIS2) is a system that contains the agregate data of all the facilities in Kenya as well as supports the addition of new community units.

DHIS-MFL Integration system is a tool that will help in minimizing the disparities between the DHIS2 facilities and community units in Kenya and those listed in the Master Facility List by ensuring that the facilities and community health units in the DHIS are consistent with those in MFL (which contains the official list of facilities in Kenya).  

Why an Integration Tool
------------------------

Both the MFL and DHIS2 health information systems allow facilities to be added into them.
However, the MFL system has the legitimate data aboout facilities and may be inconsistent with the one entered manually in the DHIS while creating organization units.
This integration tool ensures that the facilities and community units in the DHIS2 are consistent with those in the Master Facility List.

Benefits
----------





