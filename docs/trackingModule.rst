Tracking Module
================
Facilities To Be Tracked
-------------------------

This feature shows those facilities in DHIS that either have no MFL codes or have the wrong MFL codes.

Community Units To Be Tracked
---------------------------------

This feature shows those Community Health Units in DHIS that either have no MCL codes or have the wrong MCL codes.


Facilities in the Wrong Hierachy Level
---------------------------------------

This feature shows those Facilities and Community Health Units in DHIS that have the wrong hierarchy level such as Level 6, 7 etc.



