Adding Facility/Community Units
===============================
The DHIS-MFL integration tool allows facilities and community health units which are not present in DHIS but present in MFL to be added to DHIS easily.

Add Master Facilities
-----------------------
When adding facilities to DHIS;

#. Click on Facilities Menu then Add Facilities.

		.. image:: /image/addorgunits.png

#. Select the Facility to update by clicking on it

#. Select the Sub county where the Facility is located from the drop-down list and click on the add facility button after confirming that is the correct facility that has to be added to DHIS.

		.. image:: /image/add2.png



Add Community Health Units
---------------------------
When adding Community Health Units to DHIS;

#. Click on Facilities Menu then Add Facilities.

		.. image:: /image/addorgunits.png

#. Select the Facility to update by clicking on it

#. Select the Sub county where the Community Unit is located from the drop-down list and click on the add facility button after confirming that is the correct facility that has to be added to DHIS.

		.. image:: /image/add2.png

