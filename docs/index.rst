.. DHIS-MFL Integration documentation master file, created by
   sphinx-quickstart on Wed Mar  2 12:59:23 2016.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to DHIS-MFL Integration Tool
================================================


DHIS-MFL Integration system is a tool that will help in minimizing the disparities between the DHIS2 facilities and community units in Kenya and those listed in the Master Facility List. This documentation is a guide on how to use the features of the system.

The main documentation for the site is organized into a couple sections:

   * :ref:`user-docs`
   * :ref:`developer-docs`


.. _user-docs:

.. toctree::
   :caption: User Documentation
   :maxdepth: 5

   introduction
   addFacilities
   updateName
   importExcel
   viewFacilities
   trackingModule
   printReports


.. _developer-docs:
.. toctree::
   :caption: Developer's Documentation
   :maxdepth: 3
  


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

