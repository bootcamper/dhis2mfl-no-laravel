<?php
/* Represents organisation unit details in json form and updates them in dhis2 in the approporiate hierarchy*/
	include 'config.php';

	
	
	$name = "Afya Medical Clinic";
	$id = mysqli_real_escape_string($conn,$_GET['parent']['id']);
	
	$orgUnitName = array('name'=>$name);
	$jsonName = json_encode($orgUnitName);

	print_r($orgUnitName);

	print_r($jsonName);

	//Executes the curl call to update data via the API
	
	$ch = curl_init("http://localhost:8080/api/organisationUnits/".$id."/name");
    curl_setopt($ch, CURLOPT_HTTPHEADER,array('Content-Type: application/json'));
    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "PATCH");
	curl_setopt($ch, CURLOPT_POSTFIELDS,$jsonName);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);                                                                      
	curl_setopt($ch, CURLOPT_USERPWD, "$username:$password");
	$result = curl_exec($ch);

	echo $result;


	
?>