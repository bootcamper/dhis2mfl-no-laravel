<?php
$uploadedStatus = 0;
if ( isset($_POST["submit"]) )
  {
    if ( isset($_FILES["file"]))
    {
    //if there was an error uploading the file
      if ($_FILES["file"]["error"] > 0)
        {
          echo "Return Code: " . $_FILES["file"]["error"] . "<br />";
        }
        else
          {
            if (file_exists($_FILES["file"]["name"]))
              {
                unlink($_FILES["file"]["name"]);
              }
              $storagename = "Facility Export Material List";
              move_uploaded_file($_FILES["file"]["tmp_name"],  $storagename);
              $uploadedStatus = 1;
          }
    }
        else
          {
            echo "No file selected <br />";
          }
  }
?>


<html>

<head>
<title>Import Excel file </title>

</head>

<body>

<table width="600" style="margin:115px auto; background:#f8f8f8; border:1px solid #eee; padding:10px;">

<form action="" method="post" enctype="multipart/form-data">

<tr><td colspan="2" style="font:bold 21px arial; text-align:center; border-bottom:1px solid #eee; padding:5px 0 10px 0;">
<a href="#" target="_blank">Excel Import</a></td></tr>

<tr><td colspan="2" style="font:bold 15px arial; text-align:center; padding:0 0 5px 0;">Data Uploading System</td></tr>

<tr>

<td width="50%" style="font:bold 12px tahoma, arial, sans-serif; text-align:right; border-bottom:1px solid #eee; padding:5px 10px 5px 0px; border-right:1px solid #eee;">Select file</td>

<td width="50%" style="border-bottom:1px solid #eee; padding:5px;"><input type="file" name="file" id="file" /></td>

</tr>

<tr>

<td style="font:bold 12px tahoma, arial, sans-serif; text-align:right; padding:5px 10px 5px 0px; border-right:1px solid #eee;">Submit</td>

<td width="50%" style=" padding:5px;"><input type="submit" name="submit" /></td>

</tr>

</table>

<?php
if($uploadedStatus==1)
  {
    include 'config.php';
    set_include_path(get_include_path() . PATH_SEPARATOR . 'Classes/');
    include 'PHPExcel/IOFactory.php';
    try
      {
	       $objPHPExcel = PHPExcel_IOFactory::load($storagename);
      }
      catch(Exception $e)
      {
        die('Error loading file "'.pathinfo($storagename,PATHINFO_BASENAME).'": '.$e->getMessage());
      }


      $allDataInSheet = $objPHPExcel->getActiveSheet()->toArray(null,true,true,true);
      $arrayCount = count($allDataInSheet);  // Here get total count of row in that Excel sheet

      for($i=2; $i < $arrayCount; $i++)
        {
          $values[] = "('".mysqli_real_escape_string($conn, trim($allDataInSheet[$i]["A"]))."', '".mysqli_real_escape_string($conn, trim($allDataInSheet[$i]["B"]))."', '".mysqli_real_escape_string($conn, trim($allDataInSheet[$i]["D"]))."', '".mysqli_real_escape_string($conn, trim($allDataInSheet[$i]["K"]))."', '".mysqli_real_escape_string($conn, trim($allDataInSheet[$i]["L"]))."')";
          if(($i % 1000) == 0 || $i == $arrayCount)
            {
              $insertStatement = "INSERT INTO mfl_table (Name, Code, KephLevel, County, Constituency) VALUES ".implode(",",$values);
              $query = mysqli_query($conn, $insertStatement);
              $values = null;
              if($query)
                {
                  $msg = '<br><br>Record has been added. <div style="Padding:20px 0 0 0;"></div>';
                }
                else
                  {
                    $msg = '<br><br>An error has occurred. <div style="Padding:20px 0 0 0;"></div>';
                  }
            }
        }
        echo "<div style='font: bold 18px arial,verdana;padding: 45px 0 0 500px;'>".$msg."</div>"; 
  } 
  ?>

</form>

<script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-38304687-1']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>

</body>

</html>