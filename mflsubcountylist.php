<?php


include 'config.php';

$county = trim(mysqli_real_escape_string($conn,$_GET['county']));
//$county = "WAJIR";

$query = "SELECT DISTINCT(Constituency) AS sub_counties FROM dhis.mfl_facilities WHERE County = '$county' ORDER BY sub_counties ASC";

$result = mysqli_query($conn,$query);
$subCounties = mysqli_fetch_all($result,MYSQLI_ASSOC);
echo json_encode($subCounties);

?>